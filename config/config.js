function config() {
    var configEnv = "development"
    if (process.env.NODE_ENV) {
        configEnv = process.env.NODE_ENV
    }
    const config = require(`./${configEnv}.json`)
    return config
}

module.exports = config();