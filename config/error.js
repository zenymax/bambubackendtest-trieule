
var errors = {
    ERR_DB : "Database error",
    ERR_CONNECT_DB : "Please make sure Mongodb is installed and running!"
}

module.exports = errors
