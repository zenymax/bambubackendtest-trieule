var route = {}
var UserController = require("../controllers/user.controller")

const VERSION = "v1"

route.loadRoute = function(app){

    /**
     *  Get Potential Investor API with the best quality
     */
    app.route("/people-like-you").get(UserController.getPotentialInvestor)

    /**
     *  Get Potential Investor API by quality. 
     *  by add quality param in the query to get the best/good/low potential investor
     */
    app.route("/people-like-you-v2").get(UserController.getPotentialInvestorByQuality)
}

module.exports = route;

