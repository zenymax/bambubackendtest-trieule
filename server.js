////////////////////
// API Server
///////////////////

import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'

const logs = require('./utils/logs')
const route = require('./routes/route')
const dbConnection = require("./utils/dbconnection")
const config = require('./config/config')

dbConnection.connectDB(function(){
  var app = express()

  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }))
  app.use(bodyParser.json({ limit: '100mb' }));

  const path = require('path')
  app.use(express.static(path.join(__dirname, '/public')))

  app.get('/', (req, res) => {
    return res.render('index.html')
  })

  // Our routes
  route.loadRoute(app);

  process.on('uncaughtException', function (err) {
    logs.log('Global error catch: ' + err.message)
  })

  app.listen(config.port, () => {
    logs.log(`Server listening on port ${config.port}!: http://localhost:${config.port}`);
  });
})


