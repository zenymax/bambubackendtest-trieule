# Notes
Repo: https://bitbucket.org/zenymax/bambubackendtest-trieule

Tools using: VS code studio, mlab, AWS server

There are 2 APIs:
http://ec2-54-251-138-24.ap-southeast-1.compute.amazonaws.com:3000/people-like-you?age=50&latitude=40.71667&longitude=19.56667&monthlyIncome=10566&experienced=true
- Using the default quality (Good) to find out those people like you as much as possible
- I'm not using text search for the longitude and latitude due to changing those data to 2dsphere type to find the location better it could make a bug base on text search
- So that it won't return a score because I'm not using full text search

http://ec2-54-251-138-24.ap-southeast-1.compute.amazonaws.com:3000/people-like-you-v2?age=50&latitude=40.71667&longitude=19.56667&monthlyIncome=10566&experienced=true&quality=low
- Using quality input param (low, good, best) to find out those people like you as much as possible
