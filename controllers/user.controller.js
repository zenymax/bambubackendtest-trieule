'use strict';
const { createSuccessResponse, createErrorResponse } = require('../utils/gateways.js');
const { getPotentialInvestor, getPotentialInvestorByQuality } = require('../daos/user.dao.js');

var User = {}

/**
 * Get potential investor
 * @param  {} req
 * @param  {} res
 */
User.getPotentialInvestor = (req, res) => {
    var query = req.query
    console.log('[Controller] getPotentialInvestor ', query);

    getPotentialInvestor(query, (err)=>{
        createErrorResponse(res, 500, err)
    }, (doc)=>{
        createSuccessResponse(res, doc)
    })
}

/**
 * Get potential investor
 * @param  {} req
 * @param  {} res
 */
User.getPotentialInvestorByQuality = (req, res) => {
    var query = req.query
    console.log('[Controller] getPotentialInvestorByQuality ', query);

    getPotentialInvestorByQuality(query, query.quality, (err)=>{
        createErrorResponse(res, 500, err)
    }, (doc)=>{
        createSuccessResponse(res, doc)
    })
}

module.exports = User;