var assert = require('assert');
const {connectDB} = require('../../utils/dbconnection.js');
var UserDAOs= require('../../daos/user.dao')

connectDB(() => {
    
});

var query = {
    name: 'Loydie',
    age: '29',
    latitude: '40.71667',
    longitude: '19.56667',
    monthlyIncome: '6566',
    experienced: 'false'
}

var query2 = {
    age: '50',
    latitude: '40.71667',
    longitude: '19.56667',
    monthlyIncome: '10566',
    experienced: 'false',
    quality: 'low'
}

describe('GET /people-like-you', function () {
    describe('Query User Data', function () {
        it('It should return an array of json object', function (done) {
            UserDAOs.getPotentialInvestor(query, (err) => {}, (doc)=>{
                if (doc && doc.length == 1 && doc[0].name == query.name && 
                    doc[0].age == query.age) {
                    done()
                } else {
                    done("Can not find this person like you")
                } 
            })
        });
    });
})

describe('GET /people-like-you-v2', function () {
    describe('Query User Data', function () {
        it('It should return an array of json object', function (done) {
            UserDAOs.getPotentialInvestorByQuality(query2, query2.quality, (err) => {}, (doc)=>{
                if (doc && doc.length == 10) {
                    done()
                } else {
                    done("Can not find this person like you")
                } 
            })
        });
    });
})