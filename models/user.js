var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    name: String, 
    age: Number,
    latitude: String,
    longitude: String,
    monthlyIncome: Number,
    experienced: Boolean,
    score: Number,
    location: {}
},
{
	timestamps: true,
	usePushEach: true
});


module.exports = mongoose.model('user', UserSchema);