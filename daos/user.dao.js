'use strict';
const UserModel = require('../models/user.js');
const Validator = require('validator');

const FILTER_BY_SCORE = {
    "LOW": {
        INCOME_RANGE: 60,
        AGE_RANGE: 60,
        LL_RANGE: 100000
    },
    "GOOD": {
        INCOME_RANGE: 20,
        AGE_RANGE: 20,
        LL_RANGE: 40000
    },
    "BEST": {
        INCOME_RANGE: 10,
        AGE_RANGE: 10,
        LL_RANGE: 10000
    }
}

/**
 * Get the best potential investor by quality filter
 * @param  {} query
 * @param  {} error
 * @param  {} success
 */
const getPotentialInvestorByQuality = (query, filter, error, success) => {
    console.log('[DAO] getPotentialInvestorByQuality input ', query);
    if (!filter) {
        filter = 'BEST'
    } else {
        filter = filter.toUpperCase()
    }

    const INCOME_RANGE = FILTER_BY_SCORE[filter].INCOME_RANGE
    const AGE_RANGE = FILTER_BY_SCORE[filter].AGE_RANGE
    const LL_RANGE = FILTER_BY_SCORE[filter].LL_RANGE
    var optional = {}

    // validate age data and set the range for query
    if (query.age && Validator.isInt(query.age)) {
        var age = parseInt(query.age)
        optional.age = {$gte: age - age*AGE_RANGE/100, $lte: age + age*AGE_RANGE/100}
    }

    // validate longitude,latitude data and set the range for query
    if (query.latitude && query.longitude && 
        Validator.isLatLong(query.longitude+","+query.latitude)) {
        optional.location = { 
            $near : {
                $geometry : {
                    type : "Point" ,
                    coordinates : [parseFloat(query.longitude, 10), parseFloat(query.latitude, 10)] 
                },
                $maxDistance : LL_RANGE
            }
        }
    }

    // validate monthlyIncome data and set the range for query
    if (query.monthlyIncome && Validator.isInt(query.monthlyIncome)) {
        var income = parseInt(query.monthlyIncome)
        optional.monthlyIncome = {$gte: income - income*INCOME_RANGE/100, $lte: income + income*INCOME_RANGE/100}
    }

    // validate experienced data and set the range for query
    if (query.experienced && Validator.isBoolean(query.experienced)) {
        optional.experienced = (query.experienced === 'true');
    }

    console.log('optional ', optional);
    var fields = { '_id': 0, 'name': 1, 'age': 1, 'monthlyIncome': 1, 'experienced': 1, 'location.coordinates':1 };

    UserModel.find(optional).select(fields).limit(10).lean().exec((err, doc)=>{
        console.log('[DAO] getPotentialInvestorByQuality output ', err, doc);
        if (err) {
            error(err)
        } else {
            success(doc)
        }
    })
}

/**
 * Get the best potential investor
 * @param  {} query
 * @param  {} error
 * @param  {} success
 */
const getPotentialInvestor = (query, error, success) => {
    console.log('[DAO] getPotentialInvestor input ', query);
    const INCOME_RANGE = FILTER_BY_SCORE["GOOD"].INCOME_RANGE
    const AGE_RANGE = FILTER_BY_SCORE["GOOD"].AGE_RANGE
    const LL_RANGE = FILTER_BY_SCORE["GOOD"].LL_RANGE
    var optional = {}

    // validate age data and set the range for query
    if (query.age && Validator.isInt(query.age)) {
        var age = parseInt(query.age)
        optional.age = {$gte: age - age*AGE_RANGE/100, $lte: age + age*AGE_RANGE/100}
    }

    // validate longitude,latitude data and set the range for query
    if (query.latitude && query.longitude && 
        Validator.isLatLong(query.longitude+','+query.latitude)) {
        optional.location = { 
            $near : {
                $geometry : {
                    type : 'Point',
                    coordinates : [parseFloat(query.longitude, 10), parseFloat(query.latitude, 10)] 
                },
                $maxDistance : LL_RANGE
            }
        }
    }

    // validate monthlyIncome data and set the range for query
    if (query.monthlyIncome && Validator.isInt(query.monthlyIncome)) {
        var income = parseInt(query.monthlyIncome)
        optional.monthlyIncome = {$gte: income - income*INCOME_RANGE/100, $lte: income + income*INCOME_RANGE/100}
    }

    // validate experienced data and set the range for query
    if (query.experienced && Validator.isBoolean(query.experienced)) {
        optional.experienced = (query.experienced === 'true');
    }

    console.log('optional ', optional);
    var fields = { '_id': 0, 'name': 1, 'age': 1, 'monthlyIncome': 1, 'experienced': 1, 'location.coordinates':1 };

    UserModel.find(optional).select(fields).limit(10).lean().exec((err, doc)=>{
        console.log('[DAO] getPotentialInvestor output ', err, doc);
        if (err) {
            error(err)
        } else {
            success(doc)
        }
    })
}

const transformLongLatData = (data, error, success) => {
    var ops = [];
    UserModel.find({}).exec((err, docs)=>{
        if (err) {
            error(err)
        } else {
            docs.forEach(function(doc) {
                ops.push({
                    "updateOne": {
                        "filter": { "_id": doc._id },
                        "update": {
                            "$set": { 
                                "location": {
                                    "type": "Point",
                                    "coordinates": [parseFloat(doc.longitude, 10), parseFloat(doc.latitude, 10)]
                                }
                            },
                            "$unset": { "longitude": "", "latitude": "" }
                        }
                    }
                });
                if ( ops.length % 1000 == 0 ) {
                    UserModel.bulkWrite(ops);
                    ops = [];
                }
            })

        }
    })
}
// transformLongLatData()

module.exports = {
    getPotentialInvestor: getPotentialInvestor,
    getPotentialInvestorByQuality: getPotentialInvestorByQuality
}