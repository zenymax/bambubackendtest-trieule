const createErrorResponse = (res, code, message) => {
    return res.status(code || 501).json({ "message": message || "Something went wrong..."})
};

const createSuccessResponse = (res, data) => {
    return res.status(200).json({ "peopleLikeYou": data || {}})
}

module.exports = {
    createErrorResponse,
    createSuccessResponse
}