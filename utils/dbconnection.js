var config = require('../config/config');
var mongoose = require('mongoose')
var ERRORS = require('../config/error.js')
var INFO = require('../config/info.js')
var Logs = require('./logs')

module.exports.connectDB = function (startServer) {

    // Set native promises as mongoose promise
    mongoose.Promise = global.Promise

    // MongoDB Connection
    mongoose.connect(config.database_url, {useNewUrlParser: true}, (error) => {
        if (error) {
            Logs.log(ERRORS.ERR_CONNECT_DB)
            throw error
        }
        Logs.log(INFO.SU_CONNECT_DB)

        startServer()
    });
}