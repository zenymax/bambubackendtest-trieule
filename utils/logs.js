
var logs = {}

logs.log = function(text) {
    return console.log(new Date() + " - Msg: ", text);
}
logs.logQuery = function(query, file) {
    return console.log(new Date() + "\n- Query at: " + file + "\n- Query: " + query) 
}

module.exports = logs;